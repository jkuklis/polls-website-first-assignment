from django.conf.urls import url

from . import views

app_name = 'polls'

urlpatterns = [
    url(r'^$', views.HomeView.as_view(), name='home'),
    url(r'^(?P<voi_id>[0-9]+)/$', views.VoivodeshipView.as_view(),
        name="voivodeship"),
    url(r'^(?P<voi_id>[0-9]+)/(?P<county_id>[0-9]+)/$', views.CountyView.as_view(),
        name="county"),
    url(r'^(?P<voi_id>[0-9]+)/(?P<county_id>[0-9]+)/(?P<cons_id>[0-9]+)/$', views.ConstituencyView.as_view(),
        name="constituency"),
    url(r'^(?P<voi_id>[0-9]+)/(?P<county_id>[0-9]+)/(?P<cons_id>[0-9]+)/(?P<wrong>wrong)$', views.ConstituencyView.as_view(),
        name="constituency_wrong"),
    url(r'^(?P<voi_id>[0-9]+)/(?P<county_id>[0-9]+)/(?P<cons_id>[0-9]+)/votes/$', views.vote,
        name="votes"),
    url(r'^login/$', views.login_view, name='login'),
    url(r'^logout/$', views.logout_view, name='logout'),
    url(r'^register/$', views.register_view, name='register'),
    url(r'^find/$', views.find, name='find'),
    url(r'^autocomplete/$', views.Autocomplete.as_view(), name='autocomplete'),
]
