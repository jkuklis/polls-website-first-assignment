import csv
from polls.models import Voivodeship, County, District, Constituency

districts = []

with open('pkw2000.csv', 'rt', encoding='utf8') as csvfile:
    reader = csv.DictReader(csvfile)
    poll_fields = reader.fieldnames[5:23]
    candidates = reader.fieldnames[11:23]

    i = 0
    print(i)

    voivodeship = None
    county = None
    district = None

    for row in reader:
        i += 1
        print(i)

        if i == 1:
            voivodeship = Voivodeship.objects.create(name=row['Województwo'], lowest_cons_code=0)

        if voivodeship.name != row['Województwo']:
            voivodeship.highest_cons_code = i - 1
            voivodeship.save()
            voivodeship = Voivodeship.objects.create(name=row['Województwo'], lowest_cons_code=i-1)

        if i == 1:
            county = County.objects.create(
                number=row['Nr okręgu'],
                voivodeship=voivodeship,
                lowest_cons_code=0,
            )

        if county.number != row['Nr okręgu']:
            county.highest_cons_code = i - 1
            county.save()
            county = County.objects.create(
                number=row['Nr okręgu'],
                voivodeship=voivodeship,
                lowest_cons_code=i-1,
            )

        if row['Powiat'] not in districts:
            districts.append(row['Powiat'])
            district = District.objects.create(
                name=row['Powiat'],
                county=county,
            )
        else:
            district = District.objects.get(name=row['Powiat'])

        scores = []

        for candidate in candidates:
            scores.append(int(row[candidate]))

        constituency = Constituency.objects.create(
            code=row['Kod gminy'],
            name=row['Gmina'],
            allowed=row['Uprawnieni'],
            ballots=row['Karty wydane'],
            votes=row['Głosy oddane'],
            legit_votes=row['Głosy ważne'],
            spoiled_votes=row['Głosy nieważne'],
            list_of_scores=str(scores),
            district=district,
        )

    voivodeship.highest_cons_code = i
    voivodeship.save()

    county.highest_cons_code = i
    county.save()
