from django.test import TestCase, LiveServerTestCase
from django.contrib.auth.models import User
from django.test import Client
from polls.models import Voivodeship, County, District, Constituency
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.expected_conditions import staleness_of
from contextlib import contextmanager
from selenium.webdriver.common.by import By


class PollsLiveServerTest(LiveServerTestCase):
    def setUp(self):
        self.driver = webdriver.Firefox(executable_path='/home/jk/Downloads/geckodriver')
        #self.user = User.objects.create_user(username='jenny', email=None, password='jennyjenny')
        super(PollsLiveServerTest, self).setUp()

    def tearDown(self):
        self.driver.quit()
        super(PollsLiveServerTest, self).tearDown()

    @contextmanager
    def wait_for_page_load(self, timeout=30):
        old_page = self.driver.find_element_by_tag_name('html')
        yield
        WebDriverWait(self.driver, timeout).until(
            staleness_of(old_page)
        )

    def test_whole(self):
        self.driver.get('http://localhost:8000/login')
        username = self.driver.find_element_by_id('username')
        username.send_keys('jk')

        password = self.driver.find_element_by_id('password')
        password.send_keys('jokejoke')

        button = self.driver.find_element_by_id('login_button')
        button.click()

        self.driver.implicitly_wait(30)

        voi = self.driver.find_element_by_partial_link_text('dolnośląskie')
        voi.click()

        self.driver.implicitly_wait(30)

        cou = self.driver.find_element_by_partial_link_text('3')
        cou.click()

        self.driver.implicitly_wait(30)

        con = self.driver.find_element_by_partial_link_text('Ruja')
        con.click()

        self.driver.implicitly_wait(30)

        lepper = self.driver.find_element_by_id('LEPPER')
        val = lepper.get_attribute('value')
        lep1 = (val == '54')
        lep2 = (val == '420')

        self.assertTrue(lep1 or lep2)

        lepper.clear()
        if lep1:
            lepper.send_keys('420')
        else:
            lepper.send_keys('54')

        votes_button = self.driver.find_element_by_id('votes_button')
        votes_button.click()

        self.driver.implicitly_wait(30)

        if lep1:
            self.assertEquals(int(lepper.get_attribute('value')), 420)
        else:
            self.assertEquals(int(lepper.get_attribute('value')), 54)


'''
class Request:
    pass


class PollsTest(TestCase):
    c = Client()
    candidates_names = [
         "Dariusz Maciej GRABOWSKI",
         "Piotr IKONOWICZ",
         "Jarosław KALINOWSKI",
         "Janusz KORWIN-MIKKE",
         "Marian KRZAKLEWSKI",
         "Aleksander KWAŚNIEWSKI",
         "Andrzej LEPPER",
         "Jan ŁOPUSZAŃSKI",
         "Andrzej Marian OLECHOWSKI",
         "Bogdan PAWŁOWSKI",
         "Lech WAŁĘSA",
         "Tadeusz Adam WILECKI",
    ]

    @classmethod
    def setUpTestData(cls):
        User.objects.create_user(username='john', password='johnjohn')
        cls.voi = Voivodeship.objects.create(name='woj')
        cls.cou = County.objects.create(number=1, voivodeship=cls.voi)
        cls.dis = District.objects.create(name='dis', county=cls.cou)
        cls.con = Constituency.objects.create(name='con', district=cls.dis, code=1, allowed=3,
                                          ballots=2, votes=2, legit_votes=1, spoiled_votes=1,
                                          list_of_scores=[1,0,0,0,0,0,0,0,0,0,0,0])

    def test_nocontext_get(self):
        response = self.c.get('/login/')
        self.assertEqual(response.status_code, 200)

        response = self.c.get('/register/')
        self.assertEqual(response.status_code, 200)

        response = self.c.get('/autocomplete/')
        self.assertEqual(response.status_code, 200)

    def test_wrong_login(self):

        response = self.c.post('/login/', {'username': 'non', 'password': 'existing'})
        self.assertEqual(response.status_code, 200)

        self.assertIn('kombinacja', (str(response.content).split(' ')))

    def test_login(self):
        response = self.c.post('/login/', {'username': 'john', 'password': 'johnjohn'})

        self.assertEqual(response.status_code, 302)

    def test_vote_change_visibility(self):
        response = self.c.get('/1/1/1/')
        self.assertNotIn('liczby', (str(response.content).split(' ')))

        response = self.c.post('/login/', {'username': 'john', 'password': 'johnjohn'}, follow=True)
        self.assertIn('john!<br>\\n', (str(response.content).split(' ')))

        self.assertTrue(response.context['user'].is_active)

        response = self.c.get('/1/1/1/')
        self.assertIn('liczby', str(response.content).split(' '))

    def test_vote_change_positive(self):
        candidates = []
        for cand in self.candidates_names:
            surname = cand.split(' ')[-1]
            candidates.append(surname)

        context = {}
        for cand in candidates:
            context[cand] = 1

        self.c.post('/1/1/1/votes/', context, follow=True)
        self.assertEqual(Constituency.objects.all()[0].legit_votes, 12)

    def test_vote_change_negative(self):
        candidates = []
        for cand in self.candidates_names:
            surname = cand.split(' ')[-1]
            candidates.append(surname)

        context = {}
        for cand in candidates:
            context[cand] = 1

        context[candidates[-1]] = -1
        response = self.c.post('/1/1/1/votes/', context, follow=True)
        self.assertIn('con\\n', str(response.content).split(' '))

    def test_find_incorrect(self):
        response = self.c.post('/find/', {'name': 'xx'}, follow=True)
        self.assertIn('Wpisz', str(response.content).split(' '))

        response = self.c.post('/find/', {'name': ''}, follow=True)
        self.assertIn('Wpisz', str(response.content).split(' '))

    def test_find_correct(self):
        response = self.c.post('/find/', {'name': 'con'}, follow=True)
        self.assertIn('Adam', str(response.content).split(' '))
'''
