from django.shortcuts import render
from django.views.generic import TemplateView
from django.contrib.auth import logout, login, authenticate
from django.http import HttpResponseRedirect
from django.shortcuts import reverse
from django.contrib.auth.forms import UserCreationForm
from polls.models import LoginForm, Voivodeship, County, Constituency
import ast


class Unit:
    pass


class Candidate:
    pass


candidates_names = [
    "Dariusz Maciej GRABOWSKI",
    "Piotr IKONOWICZ",
    "Jarosław KALINOWSKI",
    "Janusz KORWIN-MIKKE",
    "Marian KRZAKLEWSKI",
    "Aleksander KWAŚNIEWSKI",
    "Andrzej LEPPER",
    "Jan ŁOPUSZAŃSKI",
    "Andrzej Marian OLECHOWSKI",
    "Bogdan PAWŁOWSKI",
    "Lech WAŁĘSA",
    "Tadeusz Adam WILECKI",
]


def cand_scores(lowest, highest, constituencies):
    candidates = []

    whole = 0

    for i in range(0, len(candidates_names)):
        cand = Candidate()
        cand.name = candidates_names[i]
        cand.votes = 0
        for cons in constituencies[lowest:highest]:
            scores = ast.literal_eval(cons.list_of_scores)
            cand.votes += scores[i]

        candidates.append(cand)
        whole += cand.votes

    if whole != 0:
        for cand in candidates:
            cand.score = 100 * (cand.votes / whole)
    else:
        for cand in candidates:
            cand.score = 100 / len(candidates)

    candidates = sorted(candidates, key=lambda candidate: candidate.score, reverse=True)

    for i in range(0, len(candidates)):
        candidates[i].place = i + 1

    return candidates


class HomeView(TemplateView):
    template_name = "polls/layout.html"

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['home'] = True
        context['cons'] = False
        units = []

        constituencies = Constituency.objects.all()

        for voi in Voivodeship.objects.all():
            unit = Unit()
            unit.href = voi.id
            unit.name = voi.name.lower()
            unit.legit = 0
            unit.spoiled = 0
            unit.allowed = 0

            for cons in constituencies[voi.lowest_cons_code:voi.highest_cons_code]:
                unit.legit += cons.legit_votes
                unit.spoiled += cons.spoiled_votes
                unit.allowed += cons.allowed

            if unit.allowed == 0:
                unit.attendance = 0
            else:
                unit.attendance = 100 * (unit.legit + unit.spoiled) / unit.allowed

            units.append(unit)

        candidates = cand_scores(0, len(constituencies) - 1, constituencies)

        context['candidates'] = candidates
        context['units'] = units
        context['region_area'] = "województwach"
        context['cand_area'] = "kraju"
        return context


class VoivodeshipView(TemplateView):
    template_name = "polls/layout.html"

    def get_context_data(self, **kwargs):
        context = super(VoivodeshipView, self).get_context_data(**kwargs)
        context['home'] = False
        context['cons'] = False
        context['voi_id'] = self.kwargs['voi_id']
        units = []

        voi = Voivodeship.objects.get(id=context['voi_id'])

        constituencies = Constituency.objects.all()

        for county in County.objects.filter(voivodeship=voi):
            unit = Unit()
            unit.href = county.id
            unit.name = str(county.number)
            unit.legit = 0
            unit.spoiled = 0
            unit.allowed = 0

            for cons in constituencies[county.lowest_cons_code:county.highest_cons_code]:
                unit.legit += cons.legit_votes
                unit.spoiled += cons.spoiled_votes
                unit.allowed += cons.allowed

            if unit.allowed == 0:
                unit.attendance = 0
            else:
                unit.attendance = 100 * (unit.legit + unit.spoiled) / unit.allowed

            units.append(unit)

        candidates = cand_scores(voi.lowest_cons_code, voi.highest_cons_code, constituencies)

        context['candidates'] = candidates
        context['units'] = units
        context['region_area'] = "okręgach"
        context['cand_area'] = "województwa"
        context['voi'] = voi.name.lower()
        return context


class CountyView(TemplateView):
    template_name = "polls/layout.html"

    def get_context_data(self, **kwargs):
        context = super(CountyView, self).get_context_data(**kwargs)
        context['home'] = False
        context['cons'] = False
        context['voi_id'] = self.kwargs['voi_id']
        context['county_id'] = self.kwargs['county_id']
        units = []

        county = County.objects.get(id=context['county_id'])

        constituencies = Constituency.objects.all()

        for cons in constituencies[county.lowest_cons_code:county.highest_cons_code]:
            unit = Unit()
            unit.href = cons.id
            unit.name = cons.name
            unit.legit = cons.legit_votes
            unit.spoiled = cons.spoiled_votes
            unit.allowed = cons.allowed

            if unit.allowed == 0:
                unit.attendance = 0
            else:
                unit.attendance = 100 * (unit.legit + unit.spoiled) / unit.allowed

            units.append(unit)

        candidates = cand_scores(county.lowest_cons_code, county.highest_cons_code, constituencies)

        context['candidates'] = candidates
        context['units'] = units
        context['region_area'] = "gminach"
        context['cand_area'] = "okręgu"
        context['voi'] = county.voivodeship.name.lower()
        context['county'] = county.number
        return context


class ConstituencyView(TemplateView):
    template_name = "polls/layout.html"

    def get_context_data(self, **kwargs):
        context = super(ConstituencyView, self).get_context_data(**kwargs)
        context['home'] = False
        context['cons'] = True
        context['voi_id'] = self.kwargs['voi_id']
        context['county_id'] = self.kwargs['county_id']
        context['cons_id'] = self.kwargs['cons_id']
        if 'wrong' in self.kwargs:
            context['wrong'] = self.kwargs['wrong']
        unit = Unit()

        cons = Constituency.objects.get(id=context['cons_id'])

        unit.name = cons.name
        unit.legit = cons.legit_votes
        unit.spoiled = cons.spoiled_votes
        unit.allowed = cons.allowed

        if unit.allowed == 0:
            unit.attendance = 0
        else:
            unit.attendance = 100 * (unit.legit + unit.spoiled) / unit.allowed

        candidates = []

        whole = cons.legit_votes
        scores = ast.literal_eval(cons.list_of_scores)

        for i in range(0, len(candidates_names)):
            cand = Candidate()
            cand.name = candidates_names[i]
            cand.votes = scores[i]
            cand.surname = cand.name.split(' ')[-1]

            candidates.append(cand)

        for cand in candidates:
            cand.score = 100 * (cand.votes / whole)

        candidates = sorted(candidates, key=lambda candidate: candidate.score, reverse=True)

        for i in range(0, len(candidates)):
            candidates[i].place = i + 1

        context['candidates'] = candidates
        context['units'] = [unit]
        context['region_area'] = "gminie"
        context['cand_area'] = "gminy"
        context['voi'] = Voivodeship.objects.filter(id=context['voi_id']).first().name.lower()
        context['county'] = County.objects.filter(id=context['county_id']).first().number
        context['cons'] = cons.name
        return context


def vote(request, voi_id, county_id, cons_id):
    url = reverse(
        'polls:constituency',
        kwargs={
            'voi_id': int(voi_id),
            'county_id': int(county_id),
            'cons_id': int(cons_id)
        }
    )

    cons = Constituency.objects.get(id=cons_id)
    scores = ast.literal_eval(cons.list_of_scores)

    changed_votes = {}

    for cand in candidates_names:
        surname = cand.split(' ')[-1]
        try:
            changed_votes[cand] = int(request.POST[surname])
        except ValueError:
            return HttpResponseRedirect(url)

    for cand in candidates_names:
        if changed_votes[cand] < 0:

            return HttpResponseRedirect(url + "wrong")

    change = False

    for i in range(0, len(candidates_names)):
        name = candidates_names[i]
        diff = changed_votes[name] - scores[i]
        if diff != 0:
            change = True
            cons.allowed += diff
            cons.ballots += diff
            cons.votes += diff
            cons.legit_votes += diff
            scores[i] += diff

    if change:
        cons.list_of_scores = str(scores)
        cons.save()

    return HttpResponseRedirect(url)


def login_view(request):
    form = LoginForm(request.POST or None)
    message = ""
    if request.POST and form.is_valid():
        user = form.login()
        if user:
            login(request, user)
            return HttpResponseRedirect("/")
    elif request.POST:
        message = "Zła kombinacja login-hasło"
    return render(request, 'polls/login.html', {'login_form': form, 'message': message})


def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/')


def register_view(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return HttpResponseRedirect("/")
    else:
        form = UserCreationForm()
    return render(request, 'polls/register.html', {'form': form})

class Autocomplete(TemplateView):
    template_name = "polls/search_autocomplete.html"

    def get_context_data(self, **kwargs):
        context = super(Autocomplete, self).get_context_data(**kwargs)
        cons = Constituency.objects.all()
        cons_names = []
        for con in cons:
            cons_names.append(con.name)
        context['av_tags'] = sorted(cons_names)
        return context


def find(request):
    name = request.POST['name']

    cons = Constituency.objects.filter(name=name).first()

    if cons is None:
        return render(request, 'polls/search_autocomplete.html', {'message': 'Wpisz poprawną nazwę!'})

    cons_id = cons.id

    county = cons.district.county

    if county is None:
        return render(request, 'polls/search_autocomplete.html', {'message': 'Wpisz poprawną nazwę!'})

    county_id = county.id

    voi = county.voivodeship

    if voi is None:
        return render(request, 'polls/search_autocomplete.html', {'message': 'Wpisz poprawną nazwę!'})

    voi_id = voi.id

    url = reverse(
        'polls:constituency',
        kwargs={
            'voi_id': int(voi_id),
            'county_id': int(county_id),
            'cons_id': int(cons_id)
        }
    )

    return HttpResponseRedirect(url)
