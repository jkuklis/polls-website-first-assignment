from django.db import models
from django import forms
from django.contrib.auth import authenticate


class Voivodeship(models.Model):
    name = models.CharField(max_length=40)
    lowest_cons_code = models.IntegerField(default=0)
    highest_cons_code = models.IntegerField(default=0)

    def __str__(self):
        return self.name


class County(models.Model):
    number = models.IntegerField()
    voivodeship = models.ForeignKey(Voivodeship)
    lowest_cons_code = models.IntegerField(default=0)
    highest_cons_code = models.IntegerField(default=0)

    def __str__(self):
        return str(self.number)


class District(models.Model):
    name = models.CharField(max_length=40)
    county = models.ForeignKey(County)

    def __str__(self):
        return self.name


class Constituency(models.Model):
    code = models.IntegerField()
    name = models.CharField(max_length=40)
    allowed = models.IntegerField()
    ballots = models.IntegerField()
    votes = models.IntegerField()
    legit_votes = models.IntegerField()
    spoiled_votes = models.IntegerField()

    list_of_scores = models.CharField(max_length=120, default="")

    district = models.ForeignKey(District)

    def __str__(self):
        return self.name


class LoginForm(forms.Form):
    username = forms.CharField(max_length=255, required=True)
    password = forms.CharField(widget=forms.PasswordInput, required=True)

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        if not user or not user.is_active:
            raise forms.ValidationError("Sorry, that login was invalid. Please try again.")
        return self.cleaned_data

    def login(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        return user
