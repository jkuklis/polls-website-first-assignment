from django.contrib import admin
from django import forms
import ast

# Register your models here.

from .models import Voivodeship, County, District, Constituency


candidates_names = [
    "Dariusz Maciej GRABOWSKI",
    "Piotr IKONOWICZ",
    "Jarosław KALINOWSKI",
    "Janusz KORWIN-MIKKE",
    "Marian KRZAKLEWSKI",
    "Aleksander KWAŚNIEWSKI",
    "Andrzej LEPPER",
    "Jan ŁOPUSZAŃSKI",
    "Andrzej Marian OLECHOWSKI",
    "Bogdan PAWŁOWSKI",
    "Lech WAŁĘSA",
    "Tadeusz Adam WILECKI",
]


class VoiAdminView(admin.ModelAdmin):
    search_fields = ('name',)
    list_display = ('name', 'lowest_cons_code', 'highest_cons_code')


class CouAdminView(admin.ModelAdmin):
    list_display = ('number', 'lowest_cons_code', 'highest_cons_code', 'get_voi')

    def get_voi(self, obj):
        return obj.voivodeship.name.lower()

    get_voi.short_description = "Voivodeship"
    get_voi.admin_order_field = "voivodeship__name"


class DisAdminView(admin.ModelAdmin):
    search_fields = ('name',)
    list_display = ('name', 'county', 'get_voi')

    def get_voi(self, obj):
        return obj.county.voivodeship.name.lower()
    get_voi.short_description = "Voivodeship"
    get_voi.admin_order_field = 'county__voivodeship__name'


class ConAdminView(admin.ModelAdmin):
    #form = ConAdminForm

    fields = ('name', 'code', 'district')

    def get_fields(self, request, obj=None):
        gf = super(ConAdminView, self).get_fields(request, obj)

        scores = []

        if obj is not None:
            scores = ast.literal_eval(obj.list_of_scores)
        else:
            for i in range(0, 12):
                scores.append(0)

        new_dynamic_fields = []

        for i in range(0, len(candidates_names)):
            new_field = (candidates_names[i], forms.IntegerField(initial = scores[i], min_value=0))

            new_dynamic_fields.append(new_field)

        for f in new_dynamic_fields:
            gf = gf + (f[0], )
            self.form.declared_fields.update({f[0]: f[1]})

        return gf

    search_fields = ('name', )

    list_display = ('name', 'code', 'votes', 'district', 'get_dis', 'get_cou')

    def get_dis(self, obj):
        return obj.district.county.number

    get_dis.short_description = "County"
    get_dis.admin_order_field = "district__county__number"

    def get_cou(self, obj):
        return obj.district.county.voivodeship.name.lower()

    get_cou.short_description = "Voivodeship"
    get_cou.admin_order_field = 'district__county__voivodeship__name'

    def save_model(self, request, obj, form, change):

        difference = 0

        org_scores_str = obj.list_of_scores
        org_scores = ast.literal_eval(org_scores_str)

        new_scores = []

        for i in range(0, 12):
            pass
            new_score = form.cleaned_data.get(candidates_names[i], None)

            new_scores.append(new_score)
            difference += int(new_score) - int(org_scores[i])

        obj.list_of_scores = str(new_scores)
        obj.allowed += difference
        obj.ballots += difference
        obj.votes += difference
        obj.legit_votes += difference

        obj.save()



admin.site.register(Voivodeship, VoiAdminView)
admin.site.register(County, CouAdminView)
admin.site.register(District, DisAdminView)
admin.site.register(Constituency, ConAdminView)
