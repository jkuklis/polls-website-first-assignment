# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-04-27 22:52
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Constituency',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.IntegerField()),
                ('name', models.CharField(max_length=40)),
                ('allowed', models.IntegerField()),
                ('ballots', models.IntegerField()),
                ('votes', models.IntegerField()),
                ('legit_votes', models.IntegerField()),
                ('spoiled_votes', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='County',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='District',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=40)),
                ('county_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='polls.County')),
            ],
        ),
        migrations.CreateModel(
            name='Score',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('list_of_scores', models.CharField(max_length=120)),
            ],
        ),
        migrations.CreateModel(
            name='Voivodeship',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=40)),
            ],
        ),
        migrations.AddField(
            model_name='county',
            name='voivodeship_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='polls.Voivodeship'),
        ),
        migrations.AddField(
            model_name='constituency',
            name='district_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='polls.District'),
        ),
        migrations.AddField(
            model_name='constituency',
            name='score_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='polls.Score'),
        ),
    ]
